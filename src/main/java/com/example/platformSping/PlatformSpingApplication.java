package com.example.platformSping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlatformSpingApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlatformSpingApplication.class, args);
	}

}
