package com.example.platformSping.BookProviderSQl;

import com.example.platformSping.Repository.Model.BookModel;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {
    public String insertBookSql(){
        return new SQL(){{
            /// Define SQL statement
            INSERT_INTO("tbl_book");
            VALUES("title","#{title}");
            VALUES("author","#{author}");
            VALUES("thumbnail","#{thumbnail}");
            VALUES("description","#{description}");
            VALUES("category_id","#{category.id}");
        }}.toString();
    }

    public String SelectAllBookSql(){
        return new SQL(){{
            SELECT("*");
            FROM("tbl_book");
        }}.toString();
    }


    public String selectCategoryByIdSql(int id){
        return new SQL(){{
            SELECT("*");
            FROM("tbl_book");
            INNER_JOIN("tbl_categories c ON b.category_id = c.category_id");
            WHERE("c.category_id = #{id}");
        }}.toString();
    }


    public String DeleteBookByIdSql(int id){
        return new SQL(){{
            DELETE_FROM("tbl_book");
            WHERE("id = #{id}");
        }}.toString();
    }


    public String UpdetaBookByIdSql(int id, BookModel bookModel){
        return new SQL(){{
            UPDATE("tbl_book SET title = #{bookModel.title} , author = #{bookModel.author}, thumbnail = #{bookModel.thumbnail}, description = #{bookModel.description}, category_id = #{bookModel.category.id}");
            WHERE("id = '"+id+"'");
        }}.toString();
    }


    public String SelectFilterByBookTitle(String title){
        return new SQL(){{
            SELECT("*");
            FROM("tbl_book");
            if(title !=null || !title.equals("")){
                WHERE("title like '%"+title+"%'");
            }else{
                WHERE();
            }
        }}.toString();
    }

    public String FindOneByID(int id){
        return new SQL(){{
            SELECT("*");
            FROM("tbl_book");
            WHERE("id = #{id}");
        }}.toString();
    }

}
