package com.example.platformSping.Repository;

import com.example.platformSping.BookProviderSQl.BookProvider;
import com.example.platformSping.Repository.Model.BookModel;
import com.example.platformSping.Repository.Model.CategoryModel;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {
    @InsertProvider(type = BookProvider.class,method = "insertBookSql")
    boolean insert(BookModel bookModel);
    @SelectProvider(type = BookProvider.class,method = "SelectAllBookSql")

    List<BookModel> select();

    @SelectProvider(type = BookProvider.class,method = "selectCategoryByIdSql")
    List<CategoryModel> selectCategoryById(int id);


    @DeleteProvider(type = BookProvider.class, method = "DeleteBookByIdSql")
    int DeleteBook(int id);

    @UpdateProvider(type = BookProvider.class,method = "UpdetaBookByIdSql")
    int UpdateBookById(int id , BookModel bookModel);

    @SelectProvider(type = BookProvider.class,method = "SelectFilterByBookTitle")
    List<BookModel> filterByTitle(String title);

    @SelectProvider(type = BookProvider.class,method = "FindOneByID")
    List<BookModel> FindById(int id);

}
