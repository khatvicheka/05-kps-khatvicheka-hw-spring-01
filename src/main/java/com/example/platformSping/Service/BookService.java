package com.example.platformSping.Service;

import com.example.platformSping.Repository.Model.BookModel;

import java.util.List;

public interface BookService {
    BookModel insert(BookModel bookModel);
    List<BookModel> select();
    int DeleteBook(int id);
    int UpdateBookById(int id,BookModel bookModel);
    List<BookModel> filterByTitle(String title);
    List<BookModel> FindById(int id);
}
