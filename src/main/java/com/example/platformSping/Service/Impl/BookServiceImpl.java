package com.example.platformSping.Service.Impl;

import com.example.platformSping.Repository.BookRepository;
import com.example.platformSping.Repository.Model.BookModel;
import com.example.platformSping.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookModel insert(BookModel bookModel) {
        boolean isInserted = bookRepository.insert(bookModel);
        if(isInserted){
            return bookModel;
        }else{
            return null;
        }
    }

    @Override
    public List<BookModel> select() {
        return bookRepository.select();
    }

    @Override
    public int DeleteBook(int id) {
        return bookRepository.DeleteBook(id);
    }

    @Override
    public int UpdateBookById(int id, BookModel bookModel) {
        return bookRepository.UpdateBookById(id,bookModel);
    }

    @Override
    public List<BookModel> filterByTitle(String title) {
        return bookRepository.filterByTitle(title);
    }

    @Override
    public List<BookModel> FindById(int id) {
        return bookRepository.FindById(id);
    }
}
