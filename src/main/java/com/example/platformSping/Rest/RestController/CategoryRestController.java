package com.example.platformSping.Rest.RestController;

import com.example.platformSping.Repository.Model.CategoryModel;
import com.example.platformSping.Rest.RestController.Request.CategoryRequestModel;
import com.example.platformSping.Rest.RestController.Response.BaseAPIResponse;
import com.example.platformSping.Rest.RestController.Response.ReponseMeassge;
import com.example.platformSping.Service.Impl.CategoryServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CategoryRestController {

    private CategoryServiceImpl categoryService;

    @Autowired
    public void setCategoryService(CategoryServiceImpl categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping("/category/post")
    public ResponseEntity<BaseAPIResponse<CategoryRequestModel>> insert(@RequestBody CategoryRequestModel categoryRequestModel){


        BaseAPIResponse<CategoryRequestModel> responseAIP = new BaseAPIResponse<>();
        ModelMapper modelMapper = new ModelMapper();

        CategoryModel categoryModel = modelMapper.map(categoryRequestModel,CategoryModel.class);
        CategoryModel result = categoryService.insert(categoryModel);

        CategoryRequestModel  reponse = modelMapper.map(result,CategoryRequestModel.class);

        responseAIP.setMessage("You have add book successfully");
        responseAIP.setData(reponse);
        responseAIP.setStatus(HttpStatus.OK);
        responseAIP.setTimes(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(responseAIP);
    }
    @GetMapping("/category/findAll")
    public ResponseEntity<BaseAPIResponse<List<CategoryModel>>> select(){
        ModelMapper modelMapper = new ModelMapper();
        BaseAPIResponse<List<CategoryModel>> response = new BaseAPIResponse<>();

        List<CategoryModel> resultQuery = categoryService.select();

        response.setMessage("You have found all Category successfully");
        response.setData(resultQuery);
        response.setStatus(HttpStatus.OK);
        response.setTimes(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/category/delete/{id}")
    public ResponseEntity<ReponseMeassge>  deleteById(@PathVariable int id){

        ReponseMeassge reponseMeassge = new ReponseMeassge();

        if(!"".equals(id)){
            categoryService.DeletedCategoryById(id);
            reponseMeassge.setMessage("You deleted successfully");
            reponseMeassge.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(reponseMeassge);
        }else{
            reponseMeassge.setMessage("You false to delete!");
            reponseMeassge.setStatus(HttpStatus.BAD_REQUEST);
            return ResponseEntity.ok(reponseMeassge);
        }

    }
    @PutMapping("/category/update/{id}")
    public ResponseEntity<ReponseMeassge> Update(@PathVariable int id, @RequestBody CategoryRequestModel categoryRequestModel){
        ReponseMeassge reponseMeassge = new ReponseMeassge();
        ModelMapper modelMapper = new ModelMapper();
        CategoryModel categoryModel = modelMapper.map(categoryRequestModel,CategoryModel.class);
        if(!("".equals(id)||"".equals(categoryModel))){
            categoryService.UpdateCategory(id,categoryModel);
            reponseMeassge.setMessage("You Update successfully");
            reponseMeassge.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(reponseMeassge);
        }else{
            reponseMeassge.setMessage("You Invalid to Update!");
            reponseMeassge.setStatus(HttpStatus.BAD_REQUEST);
            return ResponseEntity.ok(reponseMeassge);
        }
    }

}
