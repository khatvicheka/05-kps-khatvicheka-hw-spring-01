package com.example.platformSping.Rest.RestController.Request;

import com.example.platformSping.Repository.Model.CategoryModel;

import java.util.List;

public class BookRequestModel {
    private String title;
    private String author;
    private String thumbnail;
    private String description;
    CategoryModel category;

    public BookRequestModel() {
    }

    public BookRequestModel(String title, String author, String thumbnail, String description, CategoryModel category) {
        this.title = title;
        this.author = author;
        this.thumbnail = thumbnail;
        this.description = description;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "BookRequestModel{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", description='" + description + '\'' +
                ", category=" + category +
                '}';
    }
}
