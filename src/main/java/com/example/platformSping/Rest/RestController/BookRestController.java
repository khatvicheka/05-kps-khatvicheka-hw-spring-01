package com.example.platformSping.Rest.RestController;

import com.example.platformSping.Repository.Model.BookModel;
import com.example.platformSping.Rest.RestController.Request.BookRequestModel;
import com.example.platformSping.Rest.RestController.Response.BaseAPIResponse;
import com.example.platformSping.Rest.RestController.Response.ReponseMeassge;
import com.example.platformSping.Service.Impl.BookServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class BookRestController {
    private BookServiceImpl bookService;

    @Autowired
    public void setBookService(BookServiceImpl bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/book/post")
    public ResponseEntity<BaseAPIResponse<BookRequestModel>>insert(@RequestBody BookRequestModel bookRequestModel){

        System.out.println(bookRequestModel);
        BaseAPIResponse<BookRequestModel> responseAIP = new BaseAPIResponse<>();

        ModelMapper modelMapper = new ModelMapper();
        BookModel bookModel = modelMapper.map(bookRequestModel,BookModel.class);
//        System.out.println(bookModel);
        BookModel result = bookService.insert(bookModel);

        BookRequestModel  response = modelMapper.map(result,BookRequestModel.class);
        responseAIP.setMessage("You have add book successfully");
        responseAIP.setData(response);
        responseAIP.setStatus(HttpStatus.OK);
        responseAIP.setTimes(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(responseAIP);
    }

    
    @GetMapping("/book/findAll")
    public ResponseEntity<BaseAPIResponse<List<BookModel>>> select(){

        ModelMapper modelMapper = new ModelMapper();
        BaseAPIResponse<List<BookModel>> response = new BaseAPIResponse<>();

        List<BookModel> resultQuery = bookService.select();
//        List<BookReponseModel> bookReponseModels = new ArrayList<>();
//
//        for(BookModel bookModel:resultQuery){
//            bookReponseModels.add(modelMapper.map(bookModel,BookReponseModel.class));
//        }


        response.setMessage("You have found all Book successfully");
        response.setData(resultQuery);
        response.setStatus(HttpStatus.OK);
        response.setTimes(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/book/delete/{id}")
    public ResponseEntity<ReponseMeassge> delete(@PathVariable int id){
        ReponseMeassge reponseMeassge = new ReponseMeassge();
        if(!"".equals(id)){
            bookService.DeleteBook(id);
            reponseMeassge.setMessage("You delete successfully");
            reponseMeassge.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(reponseMeassge);
        }else{
            reponseMeassge.setMessage("You delete fail !");
            reponseMeassge.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(reponseMeassge);
        }
    }

    @PutMapping("/book/update/{id}")
    public ResponseEntity<ReponseMeassge> Update(@PathVariable int id, @RequestBody BookRequestModel bookRequestModel){
        ReponseMeassge reponseMeassge = new ReponseMeassge();
        ModelMapper modelMapper = new ModelMapper();
        BookModel bookModel = modelMapper.map(bookRequestModel,BookModel.class);
        if(!("".equals(id)||"".equals(bookModel))) {
            bookService.UpdateBookById(id, bookModel);
            reponseMeassge.setMessage("You Update successfully");
            reponseMeassge.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(reponseMeassge);
        }else{
            reponseMeassge.setMessage("You Invalid to Update!");
            reponseMeassge.setStatus(HttpStatus.BAD_REQUEST);
            return ResponseEntity.ok(reponseMeassge);
        }
    }

    @GetMapping("/book/search")
    public ResponseEntity<BaseAPIResponse<List<BookModel>>> filterByTitle(@RequestParam(required = false) String title){

        ModelMapper modelMapper = new ModelMapper();
        BaseAPIResponse<List<BookModel>> response = new BaseAPIResponse<>();

        List<BookModel> resultQuery = bookService.filterByTitle(title);
//        List<BookReponseModel> bookReponseModels = new ArrayList<>();
//
//        for(BookModel bookModel:resultQuery){
//            bookReponseModels.add(modelMapper.map(bookModel,BookReponseModel.class));
//        }


        response.setMessage("You have found by Book title");
        response.setData(resultQuery);
        response.setStatus(HttpStatus.OK);
        response.setTimes(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

    @GetMapping("/book/find")
    public ResponseEntity<BaseAPIResponse<List<BookModel>>> findById(@RequestParam(required = false) int id) {

        ModelMapper modelMapper = new ModelMapper();
        BaseAPIResponse<List<BookModel>> response = new BaseAPIResponse<>();

        List<BookModel> resultQuery = bookService.FindById(id);
//        List<BookReponseModel> bookReponseModels = new ArrayList<>();
//
//        for(BookModel bookModel:resultQuery){
//            bookReponseModels.add(modelMapper.map(bookModel,BookReponseModel.class));
//        }


        response.setMessage("You have found by Book title");
        response.setData(resultQuery);
        response.setStatus(HttpStatus.OK);
        response.setTimes(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
}
